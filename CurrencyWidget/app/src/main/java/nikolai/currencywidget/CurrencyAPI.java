package nikolai.currencywidget;

import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Mykola.Yevstihnieiev on 1/10/2017.
 */

class CurrencyAPI {
//    private static String sURL = "http://openrates.in.ua/rates";
    private static String sURL = "http://api.minfin.com.ua/nbu/7689f6c8db0f9980d7920f1bb46c299747344744/";

    static JSONObject getResponce(String parameters){
        HttpURLConnection request = null;
        JSONObject object = null;
        try {
            URL url = new URL(sURL + parameters);
            request = (HttpURLConnection) url.openConnection();
            object = new JSONObject(new BufferedReader(new InputStreamReader((InputStream) request.getContent())).readLine());
            request.disconnect();
        } catch (Exception e) {
            Log.e("ASYNC", e.getMessage());
        } finally {
            request.disconnect();
        }
        Log.e("ASYNC", object.toString());
        return object;
    }
}
