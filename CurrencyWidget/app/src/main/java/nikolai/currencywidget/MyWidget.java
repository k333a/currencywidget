package nikolai.currencywidget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by nikolai on 27.02.16.
 */
public class MyWidget extends AppWidgetProvider {
    static RemoteViews views;
    Vibrator vibrator;
    DataProvider dataStorage;

    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);
        views = createViews(context);
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        updateTextOnAllWidgets(context, getAppWidgetIds(context), appWidgetManager);
    }

    public RemoteViews createViews(Context context) {
        if (views == null) {
            return new RemoteViews(context.getPackageName(), R.layout.widget);
        } else {
            return views;
        }
    }

    public void updateTextOnAllWidgets(Context context, int[] appWidgetIds, AppWidgetManager appWidgetManager) {
        Intent clickIntent = new Intent(context, MyWidget.class).setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        clickIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
        for (int appWidgetId : appWidgetIds) {
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, appWidgetId, clickIntent, 0);
            views.setOnClickPendingIntent(R.id.widgetRoot, pendingIntent);
            views.setViewVisibility(R.id.progressBar, View.VISIBLE);
            appWidgetManager.updateAppWidget(appWidgetId, views);
        }
    }

    private int[] getAppWidgetIds(Context context) {
        return AppWidgetManager.getInstance(context).getAppWidgetIds(new ComponentName(context, MyWidget.class));
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        views = createViews(context);
        vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        if (isNetworkAvailable(context)) {
            Log.d("onUpdate ", Arrays.toString(appWidgetIds));
            enableProgressBar();
            appWidgetManager.updateAppWidget(getAppWidgetIds(context), views);
            vibrator.vibrate(150);
            new InternetConnection(context, appWidgetManager, appWidgetIds).execute();
        }
    }

    public void enableProgressBar() {
        views.setViewVisibility(R.id.progressBar, View.VISIBLE);
    }

    public void disableProgressBar() {
        views.setViewVisibility(R.id.progressBar, View.INVISIBLE);
    }


    public class InternetConnection extends AsyncTask {
        Context context;
        AppWidgetManager appWidgetManager;
        int[] appWidgetIds;

        public InternetConnection(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
            this.context = context;
            this.appWidgetManager = appWidgetManager;
            this.appWidgetIds = appWidgetIds;
        }

        @Override
        protected Object doInBackground(Object[] params) {
            dataStorage = new DataProvider();
            return dataStorage.load();
        }

        @Override
        protected void onPostExecute(Object o) {
            HashMap<String, Double> data = (HashMap<String, Double>) o;
            Log.e("OnPostExecute", o.toString());

            views.setTextViewText(R.id.USD, String.format("%.2f", data.get("USD")));
            views.setTextViewText(R.id.EUR, String.format("%.2f", data.get("EUR")));

            views.setTextViewText(R.id.differenceUSD, String.format("%.2f", data.get("DIFFUSD")));
            views.setTextViewText(R.id.differenceEUR, String.format("%.2f", data.get("DIFFEUR")));

            views.setImageViewResource(R.id.indexUSD, (data.get("DIFFUSD") >= 0) ? R.drawable.trending_up : R.drawable.trending_down);
            views.setImageViewResource(R.id.indexEUR, (data.get("DIFFEUR") >= 0) ? R.drawable.trending_up : R.drawable.trending_down);

            views.setTextViewText(R.id.date, "Last updated: " + new SimpleDateFormat("dd-MM-yyyy HH:mm").format(new Date()));
            disableProgressBar();

            appWidgetManager.updateAppWidget(getAppWidgetIds(context), views);
        }
    }
}
