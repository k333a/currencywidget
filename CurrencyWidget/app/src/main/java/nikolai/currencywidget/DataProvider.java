package nikolai.currencywidget;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Mykola.Yevstihnieiev on 1/10/2017.
 */

public class DataProvider {
    private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    private Double USD;
    private Double EUR;
    private Double DIFFUSD;
    private Double DIFFEUR;

    public Map<String, Double> load() {
        Map<String, Double> result = new HashMap<>();
        JSONObject jsonObject = CurrencyAPI.getResponce("");
        Log.e("Responce", jsonObject.toString());
        try {
            USD = jsonObject.getJSONObject("usd").getDouble("ask");
            EUR = jsonObject.getJSONObject("eur").getDouble("ask");
            DIFFUSD = jsonObject.getJSONObject("usd").getDouble("trendAsk");
            DIFFEUR = jsonObject.getJSONObject("eur").getDouble("trendAsk");
//      Get USD and EUR values
        } catch (JSONException e) {
            e.printStackTrace();
        }
        result.put("USD", validate(USD));
        result.put("EUR", validate(EUR));
        result.put("DIFFUSD", validate(DIFFUSD));
        result.put("DIFFEUR", validate(DIFFEUR));
        Log.e("Responce RESULT", result.toString());
        return result;
    }

    private Double validate(Double value){
        return (value == null) ? 0.0d : value;
    }
}